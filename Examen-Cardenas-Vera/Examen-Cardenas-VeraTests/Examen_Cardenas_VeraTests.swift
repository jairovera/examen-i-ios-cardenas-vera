//
//  Examen_Cardenas_VeraTests.swift
//  Examen-Cardenas-VeraTests
//
//  Created by Jairo Vera on 2/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import XCTest
import Alamofire
import AlamofireObjectMapper

@testable import Examen_Cardenas_Vera

class Examen_Cardenas_VeraTests: XCTestCase {
    

    func testStarPeople(){
        let personaID = arc4random_uniform(10)+1
        let URL = "https://swapi.co/api/people/\(personaID)"
        //let URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/d8bb95982be8a11a2308e779bb9a9707ebe42ede/sample_json"
        Alamofire.request(URL).responseObject { (response: DataResponse<starPeople>) in
            
            let persona = response.result.value
            print(persona?.name)
            print(persona?.birth_year)
        }
       
    }
    
    func testStartPlanet(){
        let planetaID = arc4random_uniform(10)+1
        let URL = "https://swapi.co/api/people/\(planetaID)"
        //let URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/d8bb95982be8a11a2308e779bb9a9707ebe42ede/sample_json"
        Alamofire.request(URL).responseObject { (response: DataResponse<starPlanet>) in
            
            let planeta = response.result.value
            print(planeta?.name)
            print(planeta?.climate)
            print(planeta?.diameter)
        }
    }
    
    
    
    
}
