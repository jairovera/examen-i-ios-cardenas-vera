//
//  HarrySelectCharacterViewController.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 12/10/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class HarrySelectCharacterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    var harryCharacters:[HPCharacter]?
    var selectedHarryCharacter: HPCharacter?

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.harryCharacters!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.harryCharacters![indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Personajes"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("abriendo:\(harryCharacters![indexPath.row].name)")
        self.selectedHarryCharacter = harryCharacters![indexPath.row]
        performSegue(withIdentifier: "showCharacterInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCharacterInfo" {
            if let harryCharacterView = segue.destination as? HarryCharacterInfoViewController {
                harryCharacterView.harryCharacter = self.selectedHarryCharacter
              
                
                
            }
        }
      
    }
    
    
    
    


}
