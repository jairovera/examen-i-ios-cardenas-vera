//
//  starFilms.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class starFilm: Mappable {
    
    var title: String?
    var episode_id: Int?
    var opening_crawl:String?
    var director: String?
    var producer: String?
    var release_date: String?
    var characters: [String]?
    var planets:[String]?
    var starships:[String]?
    var vehicles: [String]?
    var species: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title<-map["title"]
        episode_id<-map["episode_id"]
        opening_crawl<-map["opening_crawl"]
        director<-map["director"]
        producer<-map["producer"]
        release_date<-map["release_date"]
        /*  anidados
        characters<-map["characters"]
         planets<-map["planets"]
         starships<-map["starships"]
         vehicles<-map["vehicles"]
         species<-map["species"]
        */
        
    }
    
}

