//
//  StarWarsSelectViewController.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 11/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit

class StarWarsSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var filtro:String?
    var miStarPeople:[starPeople]?
    var miStarFilms: [starFilm]?
    var miStarPerson: starPeople?
    var miStarPelicula: starFilm?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch filtro {
        case "people"?:
            return miStarPeople!.count
        case "films"?:
            return miStarFilms!.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch filtro {
        case "people"?:
            return "Personajes"
        case "films"?:
            return "Peliculas"
        default:
            return "No hay nada que presentar :v"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text="error"
        switch filtro {
        case "people"?:
            cell.textLabel?.text = self.miStarPeople![indexPath.row].name
        case "films"?:
            cell.textLabel?.text = self.miStarFilms![indexPath.row].title
        default:
            cell.detailTextLabel?.text = "error"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch filtro {
        case "people"?:
            self.miStarPerson = self.miStarPeople![indexPath.row]
        case "films"?:
            self.miStarPelicula = self.miStarFilms![indexPath.row]
        default:
            print("filtro chafa x2")
        }
        performSegue(withIdentifier: "showStarObjectInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let starwarsObjectView = segue.destination as? StarWarsObjectInfoViewController{
            starwarsObjectView.filtro = self.filtro
            switch self.filtro{
            case "people"?:
                starwarsObjectView.miPerson = self.miStarPerson
            case "films"?:
                starwarsObjectView.miFilm = self.miStarPelicula
            default:
                print("kha")
            }
        }
    }
    
    

    
}
