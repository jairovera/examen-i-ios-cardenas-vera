//
//  HPCharacter.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 12/9/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class HPCharacter : Mappable{
    //wooooooo
    var name: String?
    var species: String?
    var gender: String?
    var house: String?
    var dateOfBirth: String?
    var yearOfBirth: String?
    var ancestry: String?
    var eyeColour: String?
    var hairColour: String?
    //var wand  anidado
    var patronus: String?
    var howartsStudent: Bool?
    var howartsStaff: Bool?
    var actor: String?
    var alive: Bool?
    var image: String?  ///   ojo aqui  llega un url
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name<-map["name"]
        species<-map["species"]
        gender<-map["gender"]
        house<-map["house"]
        dateOfBirth<-map["dateOfBirth"]
        yearOfBirth<-map["yearOfBirth"]
        ancestry<-map["ancestry"]
        eyeColour<-map["eyeColour"]
        hairColour<-map["hairColour"]
        patronus<-map["patronus"]
        howartsStudent<-map["howartsStudent"]
        howartsStaff<-map["howartsStaff"]
        actor<-map["actor"]
        alive<-map["alive"]
        image<-map["image"]
    }
}
