//
//  File.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class starPeople: Mappable{
    var name: String?
    var height: String?
    var mass: String?
    var hair_color: String?
    var skin_color: String?
    var eye_color: String?
    var birth_year: String?
    var gender: String?
    var homeworld: String?
    var films: [String]?
    var species: [String]?
    var vehicles: [String]?
    var starships: [String]?
    
    required init?(map : Map){
        
    }
    
    func mapping(map: Map){
        name<-map["name"]
        height<-map["height"]
        mass<-map["mass"]
        hair_color<-map["hair_color"]
        skin_color<-map["skin_color"]
        eye_color<-map["eye_color"]
        birth_year<-map["birth_year"]
        gender<-map["gender"]
        //homeworld anidado
        //films anidado
        //vehicles anidado
        //starships anidado
    }
    
    

}
