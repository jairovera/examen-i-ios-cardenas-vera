//
//  StarWarsObjectInfoViewController.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 11/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit

class StarWarsObjectInfoViewController: UIViewController {

    var miPerson: starPeople?
    var miFilm: starFilm?
    var filtro:String?
    
    
    
    
    @IBOutlet weak var atributoOne: UILabel!
    
    @IBOutlet weak var atributeTwo: UILabel!
    
    @IBOutlet weak var atributeThree: UILabel!
    
    @IBOutlet weak var atributeFour: UILabel!
    
    @IBOutlet weak var atributeFive: UILabel!
    
    
    
    @IBOutlet weak var valueOne: UILabel!
    
    @IBOutlet weak var valueTwo: UILabel!
    
    @IBOutlet weak var valueThree: UILabel!
    
    @IBOutlet weak var valueFour: UILabel!
    
    @IBOutlet weak var valueFive: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch filtro {
        case "people"?:
            
            atributoOne.text = "Nombre"
            valueOne.text = miPerson?.name
            
            atributeTwo.text = "Altura"
            valueTwo.text = miPerson?.height
            
            atributeThree.text = "Masa"
            valueThree.text = miPerson?.mass
            
            atributeFour.text = "Genero"
            valueFour.text = miPerson?.gender
            
            atributeFive.text = "Color de Piel"
            valueFive.text = miPerson?.skin_color
            
        case "films"?:
            
            atributoOne.text = "Titulo"
            valueOne.text = miFilm?.title
            
            atributeTwo.text = "Director"
            valueTwo.text = miFilm?.director
            
            atributeThree.text = "Productor"
            valueThree.text = miFilm?.producer
            
            atributeFour.text = "Fecha"
            valueFour.text = miFilm?.release_date
            
            atributeFive.text = "Episodio"
            valueFive.text = "\(miFilm!.episode_id ?? 0)"
            
        default:
            print("error en filtro")
        }
       
    }

  
}
