//
//  starSpecies.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class starSpecies: Mappable {
    
    var name: String?
    var classification: String?
    var designation: String?
    var average_height: String?
    var homeworld: String? //anidado
    var language: String?
    var people: [String]? //anidado
    var films: [String]? //anidado
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
    
        name<-map["name"]
        classification<-map["classification"]
        designation<-map["designation"]
        average_height<-map["average_height"]
        //homeworld<-map["homeworld"]
        language<-map["language"]
        //people<-map["people"]
        //films<-map["films"]
        
    }
    
}

