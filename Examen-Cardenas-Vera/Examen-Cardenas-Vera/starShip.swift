//
//  starShip.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper
class starShip: Mappable{
    
    var name: String?
    var model: String?
    var manufacturer: String?
    var cost_in_credits: Int?
    var length: Int?
    var max_atmosphering_speed: String?
    var crew: Int?
    var passangers: Int?
    var cargo_capacity: Int?
    var consumables: String?
    var hyperdrive_rating: Double?
    var MGLT: Int?
    var starship_class: String?
    var pilots: [String]?
    var films: [String]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
    
        name<-map["name"]
        model<-map["model"]
        manufacturer<-map["manufacturer"]
        cost_in_credits<-map["cost_in_credits"]
        length<-map["length"]
        max_atmosphering_speed<-map["max_atmosphering_speed"]
        crew<-map["crew"]
        passangers<-map["passangers"]
        cargo_capacity<-map["cargo_capacity"]
        consumables<-map["consumables"]
        hyperdrive_rating<-map["hyperdrive_rating"]
        MGLT<-map["MGLT"]
        starship_class<-map["starship_class"]
        //pilots<-map["pilots"]  anidado
        //films<-map["films"]  anidado
    }
    
    
}
