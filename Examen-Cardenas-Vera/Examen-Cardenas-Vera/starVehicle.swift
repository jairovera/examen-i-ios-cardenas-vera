//
//  starVehicle.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper
class starVehicle: Mappable {
    
    var name: String?
    var model: String?
    var manufacturer: String?
    var length: Double?
    var max_atmosphering_speed: Double?
    var crew: Int?
    var passengers: Int?
    var cargo_capacity: Int?
    var consumables: String?
    var vehicle_class: String?
    var pilots: [String]?
    var films: [String]?
    
    required init?(map: Map){
    
    }
    
    func mapping(map: Map){
        name<-map["name"]
        model<-map["model"]
        manufacturer<-map["manufacturer"]
        length<-map["length"]
        max_atmosphering_speed<-map["max_atmosphering_speed"]
        crew<-map["crew"]
        passengers<-map["passengers"]
        cargo_capacity<-map["cargo_capacity"]
        consumables<-map["consumables"]
        vehicle_class<-map["vehicle_class"]
        //pilots<-map["pilots"]  anidado
        //films<-map["films"]   anidado
    }
    
    
    
}
