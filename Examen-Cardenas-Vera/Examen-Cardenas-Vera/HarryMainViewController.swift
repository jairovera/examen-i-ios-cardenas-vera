//
//  HarryMainViewController.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 12/10/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class HarryMainViewController: UIViewController {
    
    var filtro:String?
    var harryCharacters:[HPCharacter]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK:- funciones
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentCharacters" {
            if let harrySelectCharacter = segue.destination as? HarrySelectCharacterViewController{
                harrySelectCharacter.harryCharacters = self.harryCharacters
            }
        }
    }
    
    
    @IBAction func TodosButtonPressed(_ sender: Any) {
        showCharacters(filtro: "todos")
    }
    
    @IBAction func EstudianteButtonPressed(_ sender: Any) {
        showCharacters(filtro: "students")
    }
    
    
    @IBAction func StaffButtonPressed(_ sender: Any) {
        showCharacters(filtro: "staff")
    }
    
    
    @IBAction func PorCasaButtonPressed(_ sender: Any) {
        selectCasa(message: "testeando")
    
    }
    
    private func selectCasa(message: String){
        let alertView = UIAlertController(title: "Escoge la casa", message: message, preferredStyle: .alert)
        
        alertView.addAction(newAction(titulo: "Gryffindor"))
        alertView.addAction(newAction(titulo: "Hufflepuff"))
        alertView.addAction(newAction(titulo: "Slytherin"))
        alertView.addAction(newAction(titulo: "Ravenclaw"))
        present(alertView, animated: true, completion: nil)
    }
    
    func newAction(titulo:String)-> UIAlertAction{
        let alertAction = UIAlertAction(title:titulo,style:.default){
            (action) in
            let casaFiltro = "house/"+titulo
            self.showCharacters(filtro: casaFiltro)
        }
        
        return alertAction
    }
    
    func showCharacters(filtro:String){
        var URL = "http://hp-api.herokuapp.com/api/characters"
        switch filtro {
        case "todos":
            URL+=""
        default:
            URL+="/\(filtro)"
        }
        
        Alamofire.request(URL).responseArray { (response: DataResponse<[HPCharacter]>) in
            
            let characterArray = response.result.value
            self.harryCharacters = characterArray
   
            print("array listo prro")
            self.performSegue(withIdentifier: "presentCharacters", sender: self)
        }
        
    }
    
    
    
    

   
}
