//
//  StarList.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 11/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class StarList: Mappable{
    
    var count: Int?
    var next: String?
    var previous: String?
    var results: [Any]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count<-map["count"]
        next<-map["next"]
        previous<-map["previous"]
        //results<-map["results"]
    }
    
    
    
}
