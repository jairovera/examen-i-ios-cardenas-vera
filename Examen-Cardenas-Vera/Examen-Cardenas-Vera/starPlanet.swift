//
//  starPlanet.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 5/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
import ObjectMapper

class starPlanet: Mappable {
    
    var name: String?
    var rotation_period: String?
    var orbital_period: String?
    var diameter: String?
    var climate: String?
    var gravity: String?
    var terrain: String?
    var surface_water: String?
    var population: String?
    var residents: [String]?
    var films: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name<-map["name"]
        rotation_period<-map["rotation_period"]
        orbital_period<-map["orbital_period"]
        diameter<-map["diameter"]
        climate<-map["climate"]
        gravity<-map["gravity"]
        terrain<-map["terrain"]
        surface_water<-map["surface_water"]
        population<-map["population"]
        //residents es anidado
        //residents<-map["residents"]
        //films es anidado
        //films<-map["films"]

    }
    
}
