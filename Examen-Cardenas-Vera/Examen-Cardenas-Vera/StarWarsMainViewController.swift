//
//  StarWarsMainViewController.swift
//  Examen-Cardenas-Vera
//
//  Created by Jairo Vera on 11/12/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class StarWarsMainViewController: UIViewController {

    var categoria:String?
    var miStarPeople: [starPeople]?
    var miStarFilms: [starFilm]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func peopleButtonPressed(_ sender: Any) {
        
        showStarObject(filtro: "people")
    }
    
    @IBAction func filmsButtonPressed(_ sender: Any) {
        showStarObject(filtro: "films")
    }
    
    
    func showStarObject(filtro:String){
        //performSegue(withIdentifier: "presentStarObjects", sender: self)
        
        let URL = "https://swapi.co/api/"+filtro
        switch filtro {
        case "people":
            categoria="people"
            Alamofire.request(URL).responseArray(keyPath: "results") { (response: DataResponse<[starPeople]>) in
                self.miStarPeople = response.result.value
                self.performSegue(withIdentifier: "presentStarObjects", sender: self)
            }
        case "films":
            categoria="films"
            Alamofire.request(URL).responseArray(keyPath: "results") { (response: DataResponse<[starFilm]>) in
                self.miStarFilms = response.result.value
                self.performSegue(withIdentifier: "presentStarObjects", sender: self)
            }
        default:
            print("no hay internet o yo que se alv")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "presentStarObjects" {
        
            if let starWarsSelectView = segue.destination as? StarWarsSelectViewController {
                starWarsSelectView.filtro = categoria
                switch categoria{
                case "people"?:
                    starWarsSelectView.miStarPeople =  self.miStarPeople
                case "films"?:
                    starWarsSelectView.miStarFilms = self.miStarFilms
                default:
                    print("alv")
                }
            }
        }
        
    }
    
}
